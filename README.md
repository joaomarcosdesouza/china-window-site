# Website BJDW 2014

## Setup

1. Install [Node.js and npm](http://nodejs.org/download/).
2. [Install Grunt's command line interface (CLI)
   globally](http://gruntjs.com/getting-started#installing-the-cli):
   `npm install -g grunt-cli`.
3. Run `npm install`.
4. The development code is in the
  [`src`](https://github.com/d3estudio/window/tree/master/src)

## Disclaimer

The build is not working fully automatic, because the `Modernize.load`