var tile3 = {
	tileWidth: 78,
	tileHeight: 78,
	thresholdRate: 0,
	horizontalTiles: 16, // Number of horizontal tiles
	verticalTiles: 8, // Number of vertical tiles
	lines: function(longitude, longitudeMin, longitudeS, latitude, latitudeMin, latitudeS){
		var lines = [
				[ // Fixed
					[0, 20], // P1
					[50, 0], // P2
					[100, 20], // P3
					[100, 80], // P4
					[50, 100], // P5
					[0, 80], // P6
					[0, 20],  // P7
					[50, 40],  // P8
					[100, 20]  // P9
				],[ // Fixed line
					[50, 40], // P10
					[50, 100] // P11
				],[ // Big1
					[this.map(latitude, 0, 90, -10, 12) + 25, this.map(latitude, 0, 90, -3, 5) + 30], // P12
					[this.map(latitude, 0, 90, -10, 12) + 75, this.map(latitude, 0, 90, -3, 5) + 10] // P13
				],[ // Big2
					[this.map(latitude, 0, 90, -12, 15) * -1 + 75, this.map(latitude, 0, 90, -5, 7) + 30], // P14
					[this.map(latitude, 0, 90, -12, 15) * -1 + 75, this.map(latitude, 0, 90, -5, 7) + 90], // P15
				],[ // Big3
					[0, this.map(latitude, 0, 90, -12, 15) * -1 + 50], // P16
					[50, this.map(latitude, 0, 90, -12, 15) * -1 + 70] // P17
				],[
					[this.map(latitudeMin, 0, 60, -10, 10.5) + this.map(latitude, 0, 90, -10, 12) + 48, this.map(latitudeMin, 0, 60, -4, 4.5) * -1 + this.map(latitude, 0, 90, -3, 5) + 20], // P18
					[this.map(latitudeS, 0, 60, -3, 4) + this.map(longitude, 0, 180, -15, 15) + 74, this.map(latitudeS, 0, 60, -1, 1) * -1 + this.map(longitude, 0, 180, -5, 5) * -1 + 30] // P19
				],[
					[this.map(latitude, 0, 90, -12, 15) * -1 + 75, this.map(latitudeMin, 0, 60, -16.5, 13.5) + this.map(latitude, 0, 90, -3, 5) + 60], // P20
					[50, this.map(longitude, 0, 180, -15, 15) + this.map(latitudeS, 0, 60, -6, 6) + 70] // P21
				],[
					[this.map(latitudeS, 0, 60, -5, 5)*-1 + this.map(longitude, 0, 180, -15, 15) * -1 + 25, this.map(latitudeS, 0, 60, -2, 2)*-1 + this.map(longitude, 0, 180, -7, 7) * -1 + 30], // P22
					[this.map(latitudeMin, 0, 60, -13.5, 10.5)*-1 + 25, this.map(latitudeMin, 0, 60, -5, 5)*-1 + this.map(latitude, 0, 90, -12, 15) * -1 + 60] // P23
				],[
					[this.map(longitudeMin, 0, 60, -5, 4) + this.map(latitude, 0, 90, -10, 12) + 48, this.map(longitudeMin, 0, 60, -4, 2) * -1 + this.map(latitude, 0, 90, -3, 5) + 20], // P24
					[this.map(longitudeS, 0, 60, -11, 12) + 24, this.map(longitudeS, 0, 60, -5, 4) * -1 + 10] // P25
				],[
					[this.map(latitude, 0, 90, -12, 15) * -1 + 75, this.map(longitudeMin, 0, 60, -8, 6) + this.map(latitude, 0, 90, -3, 5) + 60], // P26
					[100, this.map(longitudeS, 0, 60, -15, 15) + 50] // P27
				],[
					[this.map(longitudeMin, 0, 60, -6, 5)*-1 + 25, this.map(longitudeMin, 0, 60, -2, 2)*-1 + this.map(latitude, 0, 90, -12, 15) * -1 + 60], // P28
					[this.map(longitudeS, 0, 60, -12, 12) * -1 + 25, this.map(longitudeS, 0, 60, -5, 5) * -1 + 90] // P29
				]
			];

		return lines;
	},
	replicator: function(windowCanvas, windowContext, tileCanvas) {
		// Replicate the tile
		for(  iy=0 ; iy < this.verticalTiles + 3 ; iy++ ) {
			for(  ix=0 ; ix < this.horizontalTiles + 1 ; ix++ ) { // +1 for this module only
				// Saves the coordinate system
				windowContext.save();

				// Translation
				windowContext.translate(this.tileWidth * ix - this.tileWidth/2, this.tileHeight * iy - this.tileHeight/4);

				windowContext.translate(0, (-this.tileHeight/100 * 20) * iy);
				if(iy % 2 == 0) { // X e Y par
					windowContext.translate(this.tileWidth/2, 0);
				}

				windowContext.drawImage(tileCanvas, 0, 0);

				// Restores the coordinate system
				windowContext.restore();
			}
		}
	},
	map: function(value, low1, high1, low2, high2) {
	    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
	}
}