var tile4 = {
	tileWidth: 78,
	tileHeight: 78,
	thresholdRate: 0,
	horizontalTiles: 16, // Number of horizontal tiles
	verticalTiles: 8, // Number of vertical tiles
	lines: function(longitude, longitudeMin, longitudeS, latitude, latitudeMin, latitudeS){
		var influence1 = this.map(latitude, -90, 0, -3, 15),
			influence2 = this.map(longitudeS, 0, 60, -10, 23),
			influence3 = this.map(latitudeMin, 0, 60, -15, 15),
			influence4 = this.map(latitudeS, 0, 60, -6, 6),
			lines = [
				[

					[0, this.map(longitude, -180, 0, -10, 0) + 25], // P1
					[influence4 + this.map(longitudeMin, 0, 60, -10, 0) + 25, influence4 + this.map(longitude, -180, 0, -10, 0) + 25], // P2
					[this.map(longitudeMin, 0, 60, -10, 0) + 25, 0], // P3
				],[
					[this.map(longitudeMin, 0, 60, -10, 0) * -1 + 75,100], // P4
					[-influence4 + this.map(longitudeMin, 0, 60, -10, 0) * -1 + 75, -influence4 + this.map(longitude, -180, 0, -10, 0) * -1 + 75], // P5
					[100, this.map(longitude, -180, 0, -10, 0) * -1 + 75], // P6
				],[
					[0,100], // P7
					[influence3 + 25, -influence3 + 75] // P8
				],[
					[100,0], // P9
					[-influence3 + 75, influence3 + 25] // P10
				],[
					[0, influence2 + influence1 + 50], // P11
					[25, influence1 + 50] // P12
				],[
					[75, -influence1 + 50], // P13
					[100, -influence2 + -influence1 + 50] // P14
				],[
					[influence2 + influence1 + 50,0], // P15
					[influence1 + 50,25] // P16
				],[
					[ -influence1 + 50,75], // P17
					[ -influence2 + -influence1 + 50,100] // P18
				],[
					[influence1 + 50,25], // P16
					[-influence3 + 75, influence3 + 25], // P10
					[75, -influence1 + 50], // P13
					[ -influence1 + 50,75], // P17
					[influence3 + 25, -influence3 + 75], // P8
					[25, influence1 + 50], // P12
					[influence1 + 50,25] // P16
				],[
					[0,0],
					[100,0],
					[100,100],
					[0,100],
					[0,0]
				]
			];

		return lines;
	},
	replicator: function(windowCanvas, windowContext, tileCanvas) {
		for(  iy=0 ; iy < this.verticalTiles ; iy++ ) {
			for(  ix=0 ; ix < this.horizontalTiles ; ix++ ) {
				// Saves the coordinate system
				windowContext.save();

				if(iy % 2 > 0) { // check for odd or even vertical lines
					// Transforms the coordinate system
					windowContext.translate(0, windowCanvas.height + this.tileHeight);
					windowContext.scale(1, -1);
				}

				if(ix % 2 > 0) { // check for odd or even horizontal lines
					// Transforms the coordinate system
					windowContext.translate(windowCanvas.width + this.tileWidth, 0);
					windowContext.scale(-1, 1);
				}

				// Pastes the tile in its position
				windowContext.drawImage(tileCanvas, this.tileWidth * ix, this.tileHeight * iy);

				// Restores the coordinate system
				windowContext.restore();
			}
		}
	},
	map: function(value, low1, high1, low2, high2) {
	    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
	}
}