var tile1 = {
	tileWidth: 39,
	tileHeight: 39,
	thresholdRate: 3,
	horizontalTiles: 32, // Number of horizontal tiles
	verticalTiles: 16, // Number of vertical tiles
	lines: function(longitude, longitudeMin, longitudeS, latitude, latitudeMin, latitudeS){
		var lines = [
				[
					[-50, this.map(latitude, -90, 90, -5, 0) + 20], // P1
					[this.map(longitudeS, 0, 60, 0, 20) + 60, this.map(latitude, -90, 90, -5, 0) + 20], // P2
					[this.map(longitudeS, 0, 60, 0, 20) + 60, this.map(latitudeS, 0, 60, 0, 30) + 60], // P3
					[40,  this.map(latitudeS, 0, 60, -5, 25) + 60], // P4
					[40, this.map(latitudeS, 0, 60, -5, 25) + 40], // P5
					[this.map(longitudeMin, 0, 60, -5, 0) + 20, this.map(latitudeS, 0, 60, 0, 20) + 40], // P6
					[this.map(longitudeMin, 0, 60, -5, 0) + 20, 80], // P7
					[this.map(longitudeS, 0, 60, -50, 0) + 80, 80], // P8
					[this.map(longitudeS, 0, 60, -50, 0) + 80, 0] // P9
				],[
					[this.map(longitude, -180, 180, -15, 5) + 40, 0], // P10
					[this.map(longitude, -180, 180, -15, 5) + 40, 20], // P11
					[this.map(longitude, -180, 180, -15, 5) + 40, 0] // P10
				],[
					[this.map(longitudeS, 0, 60, -50, 0) + 80, this.map(latitudeMin, -180, 180, -40, 10) + 60], // P12
					[100, this.map(latitudeMin, -180, 180, -40, 10) + 60], // P13
					[this.map(longitudeS, 0, 60, -50, 0) + 80, this.map(latitudeMin, -180, 180, -40, 10) + 60] // P12
				],[
					[40, 80], // P14
					[40, 100], // P15
					[40, 80] // P14
				]
			];

		return lines;
	},
	replicator: function(windowCanvas, windowContext, tileCanvas) {
		// Replicate the tile
		for(  iy=0 ; iy < this.verticalTiles ; iy++ ) {
			for(  ix=0 ; ix < this.horizontalTiles ; ix++ ) {
				// Saves the coordinate system
				windowContext.save();

				if(iy % 2 > 0) { // check for odd or even vertical lines
					// Transforms the coordinate system
					windowContext.translate(0, windowCanvas.height + this.tileHeight);
					windowContext.scale(1, -1);
				}

				if(ix % 2 > 0) { // check for odd or even horizontal lines
					// Transforms the coordinate system
					windowContext.translate(windowCanvas.width + this.tileWidth, 0);
					windowContext.scale(-1, 1);
				}

				// Pastes the tile in its position
				windowContext.drawImage(tileCanvas, this.tileWidth * ix, this.tileHeight * iy);

				// Restores the coordinate system
				windowContext.restore();
			}
		}
	},
	map: function(value, low1, high1, low2, high2) {
	    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
	}
}