var tile2 = {
	tileWidth: 39,
	tileHeight: 39,
	thresholdRate: 0,
	horizontalTiles: 32, // Number of horizontal tiles
	verticalTiles: 16, // Number of vertical tiles
	lines: function(longitude, longitudeMin, longitudeS, latitude, latitudeMin, latitudeS){
		var mainPoint1 = this.map(longitude, -180, 0, -5, 15) + 20,
			mainPoint2 = this.map(latitude, 0, 90, -5, 15) + 60,
			lines = [
				[
					[0, mainPoint1], // P1
					[mainPoint1, mainPoint1], // P2
					[mainPoint1, 0], // P3
				],[
					[200, this.map(latitudeMin, 00, 60, -2, 15) + 20], // P4
					[100, this.map(latitudeMin, 00, 60, -2, 15) + 20], // P4
					[this.map(latitudeS, 00, 60, -5, 15) + 60, 20], // P5
					[this.map(latitudeS, 00, 60, -5, 15) + 60, 20], // P5
					[this.map(latitudeS, 00, 60, -5, 15) + 60, mainPoint2] // P6
				],[
					[this.map(latitudeMin, 00, 60, -2, 15) + 20,200], // P7
					[this.map(latitudeMin, 00, 60, -2, 15) + 20,100], // P7
					[this.map(longitudeMin, -60, 60, -40, 30) + 25, mainPoint2], // P8
					[this.map(latitudeS, 0, 60, -5, 15) + 60, mainPoint2], // P6
					[100,100] // P10
				],[
					[mainPoint1, mainPoint1], // P2
					[mainPoint1, mainPoint1], // P2
					[this.map(latitudeS, 00, 60, -5, 15) + 60, this.map(longitudeS, 0, 60, -20, 5) + this.map(latitude, -90, 90, -15, 0) + 60]
				]
			];

		return lines;
	},
	replicator: function(windowCanvas, windowContext, tileCanvas) {
		// Replicate the tile
		for(  iy=0 ; iy < this.verticalTiles ; iy++ ) {
			for(  ix=0 ; ix < this.horizontalTiles ; ix++ ) {
				// Saves the coordinate system
				windowContext.save();

				// Translation
				if(ix % 2 == 0 && iy % 2 == 0) { // X e Y par
					windowContext.translate(this.tileWidth * ix, this.tileHeight * iy);
				} else if(ix % 2 == 0 && iy % 2 != 0 ) {
					windowContext.translate(this.tileWidth * ix, this.tileHeight * iy);
					windowContext.rotate(270 * (Math.PI/180))
					windowContext.translate(-this.tileWidth, 0);
				} else if(ix % 2 != 0 && iy % 2 == 0){
					windowContext.translate(this.tileWidth * ix, this.tileHeight * iy);
					windowContext.rotate(90 * (Math.PI/180))
					windowContext.translate(0, -this.tileHeight);
				} else if(ix % 2 != 0 && iy % 2 != 0) {
					windowContext.translate(this.tileWidth * ix, this.tileHeight * iy);
					windowContext.rotate(180 * (Math.PI/180))
					windowContext.translate(-this.tileWidth, -this.tileHeight);
				}

				windowContext.drawImage(tileCanvas, 0, 0);

				// Restores the coordinate system
				windowContext.restore();
			}
		}
	},
	map: function(value, low1, high1, low2, high2) {
	    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
	}
}