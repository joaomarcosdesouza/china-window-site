/*
	D3 - Digital Prodution
*/

var $_parallax = $('.parallax'),
	$_intro = $('#intro'),
	$_tagline = $('#tagline'),
	$window = $(window);

$( document ).ready(function() {

		init();

		$window.bind('scroll',function(e){
	    	parallaxScroll();
		});

		/* Way Points */
		$('#tagline h2').waypoint(function(direction) {

			$(this).velocity({
			    letterSpacing: "3px",
			    opacity: 1
			}, {
			    /* Velocity's default options */
			    duration: 1000,
			    easing: 'ease-out'
			});

		}, { offset: '70%', triggerOnce: true  });


		$('.holder img').waypoint(function(direction) {

			if (direction === "down") {
				$(this).velocity({
					    scale: "1.09"
					    //translateY: "-15px"
					}, {
					    /* Velocity's default options */
					    duration: 5000,
					    easing: [0.52,0.53,0.85,0.65]
					});
			} else {
				$(this).velocity("reverse", { duration: 500 });
			}

		}, { offset: '70%' });


		var count = 0;


		$('.trigger').waypoint(function(direction) {

			$(this).velocity({
				    opacity: "1",
				    translateY: "-10px"
				}, {
				    /* Velocity's default options */
				    duration: 200,
				    delay: count * 100,
				    easing: [0.52,0.53,0.85,0.65]
				});

			count = count + 1;

		}, { offset: '60%', triggerOnce: true });

		$('.trigger2').waypoint(function(direction) {

			$(this).velocity({
				    opacity: "1",
				    translateY: "-10px"
				}, {
				    /* Velocity's default options */
				    duration: 200,
				    delay: count * 100,
				    easing: [0.52,0.53,0.85,0.65]
				});

		}, { offset: '130%', triggerOnce: true });


		$('.modulo').waypoint(function(direction) {

			$(this).animateSprite({
	    		fps: 12,
	    		loop: false
			});

		}, { offset: '70%', triggerOnce: true });


		$('.arrow').click(function() {
		  $_tagline.velocity("scroll", { duration: 750 })
		});

		$('#app-map-input').on('click focusin', function() {
		  this.value = '';
		});

});

function init() {

	$_intro.css( {
		'height'	: $window.height(),
		'position' 	: 'fixed'
	});
	$_tagline.css('height', $window.height() );
	$_tagline.find('h2').css({ 'letter-spacing': '25px', 'opacity': 0.5  })

	//$('#description').css('margin-top', '-110px');

	$('.item').css('opacity', '0');

	$_parallax.each(function(i) {
		var position = $window.height() * (i+1);
	  	$(this).css({'top': position, 'position':'absolute' });
	});

	//$window.scrollTop(0);

	/*$.getScript('js/app.min.js', function() {
	    AppClass();
	});*/

}

function parallaxScroll(){
    var scrolled = $(window).scrollTop();
    $_intro.css('top', 0-(scrolled*0.3) );
    $_tagline.css('background-position', "center "+(scrolled*0.3 - 200)+"px");
}
