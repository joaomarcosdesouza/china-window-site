/*
	D3 - Digital Prodution
*/

var AppClass = function() {
	var myApp = new AppMapClass();
	myApp.init();

	var myCanvas = new AppCanvasClass();
	myCanvas.init();

	$(document).bind('OnAppMapClassUpdate', function(_event, position) {
		myCanvas.update(position);
	});
}

var AppMapClass = function() {
	var myLatlng;
	var map;
	var searchBox;
	var socket;
	var position;
	var geocoder;
	var updateSocketTimeout = false;
	var updateSocketTime = 1000*1; //milesoconds
	var minZoomLevel = 3;
	var maxZoomLevel = 14;
	var boundSouthWestLat = -66;
	var boundNorthWestLng = -180;
	var boundNorthEastLat = 80;
	var boundSouthEastLng = 180;
	var defaultCity = 'sao_paulo'; // 'beijing' or 'sao_paulo';
	var initialLat;
	var initialLng;
	var initialZoomLevel;

	var initial = {};
	initial['sao_paulo'] = {}
	initial['sao_paulo']['initialLat'] = -23.563103;
	initial['sao_paulo']['initialLng'] = -46.673741;
	initial['sao_paulo']['initialZoomLevel'] = 11;
	initial['beijing'] = {}
	initial['beijing']['initialLat'] = 39.9996076;
	initial['beijing']['initialLng'] = 116.5046671;
	initial['beijing']['initialZoomLevel'] = 10;


	this.init = function() {
		initialLat = initial[defaultCity]['initialLat'];
		initialLng = initial[defaultCity]['initialLng'];
		initialZoomLevel = initial[defaultCity]['initialZoomLevel'];

		mapInit();
	}

	function mapInit() {
	  myLatlng = new google.maps.LatLng(initialLat, initialLng);

	  var mapOptions = {
	    center: myLatlng,
	    disableDefaultUI: true,
	    zoom: initialZoomLevel
	  };

	  geocoder = new google.maps.Geocoder();

	  map = new google.maps.Map(document.getElementById('app-map-canvas'), mapOptions);

	  google.maps.event.addListener(map, 'dragstart', function() {
	  });

	  google.maps.event.addListener(map, 'dragend', function() {
	    updateLatLng();
	  });

	  google.maps.event.addListener(map, 'drag', function() {
	    updateLatLng();
	  });

	  google.maps.event.addListener(map, 'idle', function() {
	    testBounds();
	    updateLatLng();
	  });

	  google.maps.event.addListener(map, 'zoom_changed', function() {
	    if (map.getZoom() < minZoomLevel) map.setZoom(minZoomLevel);
	    else if (map.getZoom() > maxZoomLevel) map.setZoom(maxZoomLevel);
	    updateLatLng();
	  });

	  // Create the search box and link it to the UI element.
	  var input = (document.getElementById('app-map-input'));
	  map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	  searchBox = new google.maps.places.SearchBox((input));
	  input.style.display = "block";

	  // Listen for the event fired when the user selects an item from the
	  // pick list. Retrieve the matching places for that item.
	  google.maps.event.addListener(searchBox, 'places_changed', function() {
	    var places = searchBox.getPlaces();
	    if (places.length == 0) return;

	    var bounds = new google.maps.LatLngBounds();
	    bounds.extend(places[0].geometry.location);
	    map.fitBounds(bounds);
	    //map.panTo(new google.maps.LatLng(places[0].geometry.location.lat(), places[0].geometry.location.lng()));
	  });

	  updateLatLng();
	}

	function updateLatLng() {
	  var mapPosition = map.getCenter();
	  var _lat = mapPosition.lat();
	  var _lng = mapPosition.lng();
	  var _quadrant = 0;
	  var _latD;
	  var _lngD;
	  if (_lat > 0) {
	    if (_lng < 0) {
	      _quadrant = 1;
	      _latD = 'N';
	      _lngD = 'W';
	    } else {
	      _quadrant = 2;
	      _latD = 'N';
	      _lngD = 'E';
	    }
	  } else {
	    if (_lng < 0) {
	      _quadrant = 3;
	      _latD = 'S';
	      _lngD = 'W';
	    } else {
	      _quadrant = 4;
	      _latD = 'S';
	      _lngD = 'E';
	    }
	  }

	  position = {quadrant: _quadrant, lat: decToDeg('lat', _quadrant, _lat), lng: decToDeg('lng', _quadrant, _lng)};

	  $('#coordinates').html(decToDegFormatted('lat', _quadrant, _lat) + _latD + '<br>' + decToDegFormatted('lng', _quadrant, _lng) + _lngD);

	  clearTimeout(updateSocketTimeout);
	  updateSocketTimeout = setTimeout(triggerUpdate, updateSocketTime);

	}

	function testBounds()
	{
	  var c = map.getCenter(),
	      x = c.lng(),
	      y = c.lat();

	  if (y < boundSouthWestLat) y = boundSouthWestLat;
	  if (y > boundNorthEastLat) y = boundNorthEastLat;
	  if (x < boundNorthWestLng) x = boundNorthWestLng;
	  if (x > boundSouthEastLng) x = boundSouthEastLng;

	  if(c.lng() !== x || c.lat() !== y)
	    map.panTo(new google.maps.LatLng(y, x));
	}

	function codeLatLng(_lat, _lng) {
	  var lat = parseFloat(_lat);
	  var lng = parseFloat(_lng);
	  var latlng = new google.maps.LatLng(lat, lng);
	  var address = false;
	  geocoder.geocode({'latLng': latlng}, function(results, status) {
	    if (status == google.maps.GeocoderStatus.OK) {
	      if(results.length > 0) {
	        try {
	          results.reverse();
	          if(results.length >= 3)
	            address = results[2].formatted_address;
	          else if(results.length >= 2)
	            address = results[1].formatted_address;
	          else
	            address = results[0].formatted_address;
	        } catch(err) {
	          console.log( err );
	        }
	      }
	    }
	    if (address)
	      console.log( address );
	  });
	}

	function decToDegFormatted(latOrLng, quadrant, dec)
	{
	  var data = decToDeg(latOrLng, quadrant, dec);
	  return Math.abs(data[0])+" "+data[1]+"' "+data[2]+"\"";
	}

	function decToDeg(latOrLng, quadrant, dec)
	{
	  var str="";
	  var deg=0, mnt=0, sec=0;
	  dec=Math.abs(dec);
	  deg=Math.floor(dec);
	  dec=(dec-Math.floor(dec))*60;
	  mnt=Math.floor(dec);
	  dec=(dec-Math.floor(dec))*60;
	  sec=Math.floor(dec*100)/100;

	  if ((latOrLng == 'lat' && (quadrant == 3 || quadrant == 4)) || (latOrLng == 'lng' && (quadrant == 1 || quadrant == 3))) {
	    deg = deg * -1;
	  }

	  return Array(deg, mnt, parseInt(sec));
	}

	function triggerUpdate() {
		$( document ).trigger( 'OnAppMapClassUpdate', position );
	}

	function getURLParameter(name) {
	    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
	}
}



/*
    CANVAS SCRIPT
*/

var AppCanvasClass = function() {

	// Basic
	var windowCanvas;
	var windowContext;
	var tileCanvas;
	var tileContext;

	// Window
	var windowWidth;
	var windowHeight;

	// Animation control
	var tile; // Keep in memory the tile that will be used for the update
	var updating; // Detects when the update is being made, will make it stop when this is false
	var params; // Used the value tween
	var targetParameters; // Used as the target to the tween
	var animationTime; // Transition animation duration

	// The big bang!
	this.init = function() {
	  var d = document;

	  // Get a reference to the canvas object
	  windowCanvas = d.getElementById("app-window-canvas");
	  windowContext = windowCanvas.getContext("2d");

	  // Create a new canvas only for the tile
	  tileCanvas = d.createElement("canvas");
	  tileContext = tileCanvas.getContext("2d");

	  // Canvas sizes
	  windowWidth = 1248,
	  windowHeight = 624;

	  // Basic setup for first render
	  params = {
	    longitude: 0,
	    longitudeMin: 0,
	    longitudeS: 0,
	    latitude: 0,
	    latitudeMin: 0,
	    latitudeS: 0
	  };
	  targetParameters = {
	    longitude: 0,
	    longitudeMin: 0,
	    longitudeS: 0,
	    latitude: 0,
	    latitudeMin: 0,
	    latitudeS: 0,
	    quadrant: 0
	  }

	  // Animation control
	  animationTime = 1000;

	  // Dimensions for the window canvas
	  windowCanvas.width = windowWidth;
	  windowCanvas.height = windowHeight;

	  // First render
	  setUpAnimation();
	};

	this.update = function(position) {
	    targetParameters.longitude = parseInt(position.lng[0]),
	    targetParameters.longitudeMin = parseInt(position.lng[1]),
	    targetParameters.longitudeS = parseInt(position.lng[2]),
	    targetParameters.latitude = parseInt(position.lat[0]),
	    targetParameters.latitudeMin = parseInt(position.lat[1]),
	    targetParameters.latitudeS = parseInt(position.lat[2]);
	    targetParameters.quadrant = parseInt(position.quadrant);

	    setUpAnimation();
	}


	// Same thing as Processing map function
	function map(value, low1, high1, low2, high2) {
	    return low2 + (high2 - low2) * (value - low1) / (high1 - low1);
	}

	// Redefines every segment on each frame
	function tileUpdate(longitude, longitudeMin, longitudeS, latitude, latitudeMin, latitudeS){
	  var multiplierX = tile.tileWidth/100.0, // Used to draw the tile proportionally to its size
	    multiplierY = tile.tileHeight/100.0, // Used to draw the tile proportionally to its size
	    points = []; // For the threshold method
	    lines = tile.lines(longitude, longitudeMin, longitudeS, latitude, latitudeMin, latitudeS), // Stores every line
	    thresholdRate = tile.thresholdRate;

	  // Makes each segment relative to the tile width and draw them
	  for ( i = 0; i < lines.length; i++) {
	    for( s = 0; s < lines[i].length; s++) {
	      // Make the relative
	      lines[i][s][0] *= multiplierX;
	      lines[i][s][1] *= multiplierY;

	      // Push all points to the variable "points"
	      // that will be analized by the threshold
	      var point = [ lines[i][s][0], lines[i][s][1] ];
	      points.push(point);
	    };
	  };

	  // Threshold
	  // Problems:
	    // 1 - The first elements have a bigger influence than the others
	    // 2 - It also blocks the lines inclination
	  if( thresholdRate != 0 ) {
	    for ( i = 0; i < points.length; i++) { // For every point in the array
	      for ( s = 0; s < points.length; s++) { // Compares this to all the others
	        var diffX = points[i][0] - points[s][0]; // X difference between them
	        var diffY = points[i][1] - points[s][1]; // Y difference between them

	        if( diffX < thresholdRate && diffX > thresholdRate * -1 && diffX != 0) {
	          var averageX = (points[i][0] + points[s][0]) / 2;
	          points[i][0] = averageX;
	          points[s][0] = averageX;
	        }

	        if( diffY < thresholdRate && diffY > thresholdRate * -1 && diffY != 0) {
	          var averageY = (points[i][1] + points[s][1]) / 2;
	          points[i][1] = averageY;
	          points[s][1] = averageY;
	        }
	      };
	    };
	  }

	  // Redefines lines arrays after threshold proccess
	  for ( i = 0; i < lines.length; i++) {
	    lines[i] = points.slice(0, lines[i].length);
	    points.splice(0, lines[i].length);
	  };

	  // Draw lines
	  for ( i = 0; i < lines.length; i++) {
	    for( s = 0; s < lines[i].length; s++) {
	      if(s == 0) { // If it is a new line
	        tileContext.stroke(); // Stroke the last one
	        tileContext.beginPath(); // Begin another
	        tileContext.moveTo(lines[i][s][0], lines[i][s][1]);
	      } else {
	        tileContext.lineTo(lines[i][s][0], lines[i][s][1]);
	      }
	    };
	  };
	}

	// Set up parameters for animation
	function setUpAnimation(){
	  // Set updating to true
	  updating = true;

	  // Defines which tile to be rendered
	  // _______________________
	  // |          |          |
	  // |     2    |     3    |  <- World map
	  // |__________|__________|
	  // |          |          |
	  // |     4    |     1    |
	  // |__________|__________|
	  if( targetParameters.latitude >= 0 && targetParameters.longitude <= 0) {
	    tile = tile2;
	  } else if( targetParameters.latitude >= 0 && targetParameters.longitude >= 0) {
	    tile = tile3;
	  } else if( targetParameters.latitude <= 0 && targetParameters.longitude <= 0) {
	    tile = tile4;
	  } else if( targetParameters.latitude <= 0 && targetParameters.longitude >= 0) {
	    tile = tile1;
	  }

	  // Dimensions for the tile canvas
	  if( tileCanvas.width != tile.tileWidth || tileCanvas.height != tile.tileHeight ) {
	    tileCanvas.width = tile.tileWidth;
	    tileCanvas.height = tile.tileHeight;

	    // Visuals
	    tileContext.strokeStyle = "#656563";
	    tileContext.lineJoin = "miter";
	    tileContext.lineWidth = 4;
	    tileContext.miterLimit=2;
	  }

	  // Start animation
	  requestAnimationFrame(changeAnimation);

	  // Start tween
	  var animation = new TWEEN.Tween(params)
	    .to({
	      longitude: targetParameters.longitude,
	      longitudeMin: targetParameters.longitudeMin,
	      longitudeS: targetParameters.longitudeS,
	      latitude: targetParameters.latitude,
	      latitudeMin: targetParameters.latitudeMin,
	      latitudeS: targetParameters.latitudeS
	     }, animationTime)
	    .easing(TWEEN.Easing.Cubic.Out) // Ease
	    .onComplete(function() {
	      // Sets update back to false, stops the requestAnimationFrame
	      updating = false;
	    })
	    .start(); // Play!
	}

	// Replicates the tile all over the window
	function changeAnimation() {
	  // Clear both canvas
	  windowContext.clearRect(0, 0, windowWidth, windowHeight);
	  tileContext.clearRect(0, 0, tileCanvas.width, tileCanvas.height);

	  // Updates tile
	  tileUpdate(params.longitude, params.longitudeMin, params.longitudeS, params.latitude, params.latitudeMin, params.latitudeS);

	  // Replicates the tile
	  tile.replicator(windowCanvas, windowContext, tileCanvas);

	  // TWEEN
	  TWEEN.update();

	  if(updating) { // Will not pass when the tween is complete
	    // Call another frame
	    requestAnimationFrame(changeAnimation);
	  }
	}
}
